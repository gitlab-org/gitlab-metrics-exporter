package promclient

import (
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
)

func Test_CounterFamilyToMetricGroup(t *testing.T) {
	c := prometheus.NewCounterVec(prometheus.CounterOpts{
		Name:      "buzz_total",
		Help:      "Counts buzzes",
		Namespace: "test",
		Subsystem: "mapping",
	}, []string{"lbl"})
	reg := prometheus.NewRegistry()
	reg.Register(c)

	c.WithLabelValues("A").Inc()
	c.WithLabelValues("B").Inc()
	c.WithLabelValues("B").Inc()

	mfs, err := reg.Gather()
	require.NoError(t, err)
	require.Len(t, mfs, 1)

	mgroup, err := MetricFamilyToMetricGroup(mfs[0])
	require.NoError(t, err)
	require.Equal(t, probe.MetricTypeCounter, mgroup.Type)
	require.Equal(t, "Counts buzzes", mgroup.Help)
	require.ElementsMatch(t, probe.SampleSlice{
		{
			FamilyName: "test_mapping_buzz_total",
			MetricName: "test_mapping_buzz_total",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzz_total",
			MetricName: "test_mapping_buzz_total",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}},
			Value:      2.0,
		},
	}, mgroup.Samples)
}

func Test_GaugeFamilyToMetricGroup(t *testing.T) {
	g := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name:      "buzzes",
		Help:      "Measures buzzes",
		Namespace: "test",
		Subsystem: "mapping",
	}, []string{"lbl"})
	reg := prometheus.NewRegistry()
	reg.Register(g)

	g.WithLabelValues("A").Set(1)
	g.WithLabelValues("B").Set(2)

	mfs, err := reg.Gather()
	require.NoError(t, err)
	require.Len(t, mfs, 1)

	mgroup, err := MetricFamilyToMetricGroup(mfs[0])
	require.NoError(t, err)
	require.Equal(t, probe.MetricTypeGauge, mgroup.Type)
	require.Equal(t, "Measures buzzes", mgroup.Help)
	require.ElementsMatch(t, probe.SampleSlice{
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}},
			Value:      2.0,
		},
	}, mgroup.Samples)
}

func Test_HistogramFamilyToMetricGroup(t *testing.T) {
	g := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:      "buzzes",
		Help:      "Observes buzzes",
		Namespace: "test",
		Subsystem: "mapping",
		Buckets:   []float64{1},
	}, []string{"lbl"})
	reg := prometheus.NewRegistry()
	reg.Register(g)

	g.WithLabelValues("A").Observe(1)
	g.WithLabelValues("B").Observe(1)
	g.WithLabelValues("B").Observe(2)

	mfs, err := reg.Gather()
	require.NoError(t, err)
	require.Len(t, mfs, 1)

	mgroup, err := MetricFamilyToMetricGroup(mfs[0])
	require.NoError(t, err)
	require.Equal(t, probe.MetricTypeHistogram, mgroup.Type)
	require.Equal(t, "Observes buzzes", mgroup.Help)
	require.ElementsMatch(t, probe.SampleSlice{
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_count",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_count",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}},
			Value:      2.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_sum",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_sum",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}},
			Value:      3.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_bucket",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}, {Name: "le", Value: "1"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_bucket",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}, {Name: "le", Value: "+Inf"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_bucket",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}, {Name: "le", Value: "1"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_bucket",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}, {Name: "le", Value: "+Inf"}},
			Value:      2.0,
		},
	}, mgroup.Samples)
}

func Test_SummaryFamilyToMetricGroup(t *testing.T) {
	g := prometheus.NewSummaryVec(prometheus.SummaryOpts{
		Name:       "buzzes",
		Help:       "Observes buzzes",
		Namespace:  "test",
		Subsystem:  "mapping",
		Objectives: map[float64]float64{0.5: 1, 0.99: 2},
	}, []string{"lbl"})
	reg := prometheus.NewRegistry()
	reg.Register(g)

	g.WithLabelValues("A").Observe(1)
	g.WithLabelValues("B").Observe(1)
	g.WithLabelValues("B").Observe(2)

	mfs, err := reg.Gather()
	require.NoError(t, err)
	require.Len(t, mfs, 1)

	mgroup, err := MetricFamilyToMetricGroup(mfs[0])
	require.NoError(t, err)
	require.Equal(t, probe.MetricTypeSummary, mgroup.Type)
	require.Equal(t, "Observes buzzes", mgroup.Help)
	require.ElementsMatch(t, probe.SampleSlice{
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_count",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_count",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}},
			Value:      2.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_sum",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes_sum",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}},
			Value:      3.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}, {Name: "quantile", Value: "0.5"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "A"}, {Name: "quantile", Value: "0.99"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}, {Name: "quantile", Value: "0.5"}},
			Value:      1.0,
		},
		{
			FamilyName: "test_mapping_buzzes",
			MetricName: "test_mapping_buzzes",
			Labels:     probe.LabelSlice{{Name: "lbl", Value: "B"}, {Name: "quantile", Value: "0.99"}},
			Value:      2.0,
		},
	}, mgroup.Samples)
}
