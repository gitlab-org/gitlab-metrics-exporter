package self

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/testhelpers"
)

func Test_ProbeSelfMetrics(t *testing.T) {
	p := NewProbe()

	result, err := probe.Await(p)
	require.NoError(t, err)
	require.NotEmpty(t, result)

	metricFamilyNames := make(map[string]struct{})
	for _, mgroup := range result {
		samples := mgroup.Samples
		for _, s := range samples {
			metricFamilyNames[s.FamilyName] = struct{}{}
		}
	}

	// Sanity check for a ProcessCollector metric.
	// Skip for OSX because /proc is not available
	if testhelpers.ProcDirIsPresent() {
		require.Contains(t, metricFamilyNames, "process_cpu_seconds_total")
	}

	// Sanity check for a GoCollector metric.
	require.Contains(t, metricFamilyNames, "go_gc_duration_seconds")
}
