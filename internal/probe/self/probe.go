package self

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/promclient"
)

type selfProbe struct{}

func NewProbe() probe.Probe {
	return &selfProbe{}
}

func (p *selfProbe) Probe(ch chan<- probe.ProbeResult) error {
	result, err := prometheus.DefaultGatherer.Gather()
	if err != nil {
		return err
	}

	for _, mf := range result {
		group, err := promclient.MetricFamilyToMetricGroup(mf)

		if err != nil {
			ch <- probe.ProbeResult{Error: err}
		} else {
			ch <- probe.ProbeResult{MGroup: *group}
		}
	}

	return nil
}

func (p *selfProbe) String() string {
	return "self"
}
