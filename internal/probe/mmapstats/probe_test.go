package mmapstats

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/testhelpers"
)

func Test_ProbeMmapStatsMetrics(t *testing.T) {
	mmapDir := testhelpers.TestDir + "/data/mmap/"
	p := NewProbe(mmapDir)

	result, err := probe.Await(p)
	require.NoError(t, err)
	require.NotEmpty(t, result)

	require.Equal(t, 2, len(result))
	testCorrectFilesMGroup(t, result[0])
	testCorrectFilesSizeMGroup(t, result[1])
}

func testCorrectFilesMGroup(t *testing.T, mgroup probe.MetricGroup) {
	require.Equal(t, "gauge", mgroup.Type)
	require.Equal(t, mmapFilesMetricHelp, mgroup.Help)

	expectedSamples := probe.SampleSlice{
		probe.Sample{
			FamilyName: mmapFilesMetricName,
			MetricName: mmapFilesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "counter",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "livesum",
				},
			},
			Value: 2,
		},
		probe.Sample{
			FamilyName: mmapFilesMetricName,
			MetricName: mmapFilesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "gauge",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "all",
				},
			},
			Value: 2,
		},
		probe.Sample{
			FamilyName: mmapFilesMetricName,
			MetricName: mmapFilesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "gauge",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "livesum",
				},
			},
			Value: 2,
		},
		probe.Sample{
			FamilyName: mmapFilesMetricName,
			MetricName: mmapFilesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "gauge",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "max",
				},
			},
			Value: 2,
		},
		probe.Sample{
			FamilyName: mmapFilesMetricName,
			MetricName: mmapFilesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "gauge",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "min",
				},
			},
			Value: 2,
		},
		probe.Sample{
			FamilyName: mmapFilesMetricName,
			MetricName: mmapFilesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "histogram",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "livesum",
				},
			},
			Value: 1,
		},
	}

	assert.ElementsMatch(t, expectedSamples, mgroup.Samples)
}

func testCorrectFilesSizeMGroup(t *testing.T, mgroup probe.MetricGroup) {
	require.Equal(t, "gauge", mgroup.Type)
	require.Equal(t, mmapFilesSizeBytesMetricHelp, mgroup.Help)

	expectedSamples := probe.SampleSlice{
		probe.Sample{
			FamilyName: mmapFilesSizeBytesMetricName,
			MetricName: mmapFilesSizeBytesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "counter",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "livesum",
				},
			},
			Value: 8192,
		},
		probe.Sample{
			FamilyName: mmapFilesSizeBytesMetricName,
			MetricName: mmapFilesSizeBytesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "gauge",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "all",
				},
			},
			Value: 8192,
		},
		probe.Sample{
			FamilyName: mmapFilesSizeBytesMetricName,
			MetricName: mmapFilesSizeBytesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "gauge",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "livesum",
				},
			},
			Value: 8192,
		},
		probe.Sample{
			FamilyName: mmapFilesSizeBytesMetricName,
			MetricName: mmapFilesSizeBytesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "gauge",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "max",
				},
			},
			Value: 8192,
		},
		probe.Sample{
			FamilyName: mmapFilesSizeBytesMetricName,
			MetricName: mmapFilesSizeBytesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "gauge",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "min",
				},
			},
			Value: 8192,
		},
		probe.Sample{
			FamilyName: mmapFilesSizeBytesMetricName,
			MetricName: mmapFilesSizeBytesMetricName,
			Labels: probe.LabelSlice{
				probe.Label{
					Name:  "metric_type",
					Value: "histogram",
				},
				probe.Label{
					Name:  "aggregation",
					Value: "livesum",
				},
			},
			Value: 4096,
		},
	}

	assert.ElementsMatch(t, expectedSamples, mgroup.Samples)
}
