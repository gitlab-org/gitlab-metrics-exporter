package mmap

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/probe"
)

func Test_DecodeEmptyMetricKey(t *testing.T) {
	encoded := `["name1","name2",[],[]]`
	expected := probe.Sample{
		FamilyName: "name1",
		MetricName: "name2",
	}

	var sample probe.Sample
	err := decodeMetric(&sample, encoded, "")
	require.NoError(t, err)

	require.Equal(t, expected, sample)
}

func Test_DecodeFullMetricKey(t *testing.T) {
	encoded := `["name1","name2",["a","b","c","d"],["str",1,true,null]]`
	expected := probe.Sample{
		FamilyName: "name1",
		MetricName: "name2",
		Labels: []probe.Label{
			{Name: "a", Value: "str"},
			{Name: "b", Value: "1"},
			{Name: "c", Value: "true"},
			{Name: "d", Value: ""},
		},
	}

	var sample probe.Sample
	err := decodeMetric(&sample, encoded, "")
	require.NoError(t, err)

	require.Equal(t, expected, sample)
}

func Test_DecodeMetricKeyWithPidLabelRetainsPid(t *testing.T) {
	encoded := `["name1","name2",["a","pid"],["str","my_proc"]]`
	expected := probe.Sample{
		FamilyName: "name1",
		MetricName: "name2",
		Labels: []probe.Label{
			{Name: "a", Value: "str"},
			{Name: "pid", Value: "my_proc"},
		},
	}

	var sample probe.Sample
	err := decodeMetric(&sample, encoded, "")
	require.NoError(t, err)
	require.Equal(t, expected, sample)
}

func Test_DecodeMetricKeyWithPidLabelAndGivenPidRetainsPid(t *testing.T) {
	encoded := `["name1","name2",["a","pid"],["str","my_proc"]]`
	expected := probe.Sample{
		FamilyName: "name1",
		MetricName: "name2",
		Labels: []probe.Label{
			{Name: "a", Value: "str"},
			{Name: "pid", Value: "my_proc"},
		},
	}

	var sample probe.Sample
	err := decodeMetric(&sample, encoded, "pid")
	require.NoError(t, err)
	require.Equal(t, expected, sample)
}

func Test_DecodeMetricKeyWithoutPidLabelAndGivenPidUsesGivenPid(t *testing.T) {
	encoded := `["name1","name2",["a"],["str"]]`
	expected := probe.Sample{
		FamilyName: "name1",
		MetricName: "name2",
		Labels: []probe.Label{
			{Name: "a", Value: "str"},
			{Name: "pid", Value: "pid"},
		},
	}

	var sample probe.Sample
	err := decodeMetric(&sample, encoded, "pid")
	require.NoError(t, err)
	require.Equal(t, expected, sample)
}
