package mmap

import (
	"encoding/binary"
	"errors"
	"io"
	"math"
	"os"
	"path"
	"regexp"
	"strings"

	"gitlab.com/gitlab-org/labkit/log"
)

const (
	entryHeaderLen = 4

	AggregateAll = "all"
	AggregateSum = "livesum"
	AggregateMin = "min"
	AggregateMax = "max"

	averageSampleSize = 200
)

type FileMetadata struct {
	MetricType  string
	Aggregation string
	Pid         string
}

type MetricFile struct {
	FileMetadata
	ContentLen uint32
	Samples    []encodedSample
}

type encodedSample struct {
	json  string
	value float64
}

var fileRegexp = regexp.MustCompile(`^(counter|gauge|histogram|summary)_(all|livesum|min|max)?_?(\w+)(-\d+)\.db$`)

// ParseFile parses prometheus-client-mmap files.
// These are binary files encoded as follows:
//
// Metric file header:
//
//	 	4B: Length of metrics data in file.
//		4B: Padding.
//
// Metric file data:
//
//	 	sample_0
//		sample_1
//		...
//		sample_N
//
// Where each sample is encoded as:
//
//	4B: Sample length n.
//	nB: Sample data (JSON encoded, 8 byte aligned). }
func ParseFile(filePath string) (*MetricFile, error) {
	fileMetadata, err := metaDataFromPath(filePath)
	if err != nil {
		return nil, err
	}
	log.WithField("filepath", filePath).
		WithField("aggregation", fileMetadata.Aggregation).
		WithField("metricType", fileMetadata.MetricType).
		WithField("pid", fileMetadata.Pid).
		Debug("ParseFile")

	f, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = f.Close()
	}()

	// Read first 4 bytes - length of metric data in file.
	var contentLen uint32
	err = binary.Read(f, binary.LittleEndian, &contentLen)
	if err != nil {
		return nil, err
	}

	// Actual metrics data follows (this contains 4B of padding we need to remove later).
	const padding = 4
	var sb strings.Builder
	sb.Grow(int(contentLen + padding))
	_, err = io.CopyN(&sb, f, int64(contentLen+padding))
	if err != nil {
		return nil, err
	}

	content := sb.String()
	offset := uint64(padding)
	samples := make([]encodedSample, 0, contentLen/averageSampleSize)

	for offset < uint64(contentLen) {
		nextOffset, sample, err := readNextSample(offset, content)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		samples = append(samples, sample)
		offset = nextOffset
	}

	return &MetricFile{*fileMetadata, contentLen, samples}, nil
}

// Read metric type, aggregation and pid from file name.
// For gauges, filenames contain an aggregation function,
// for other metrics it does not:
// - gauge_livesum_my_pid-0.db
// - counter_my_pid-0.db
func metaDataFromPath(filePath string) (*FileMetadata, error) {
	_, fileName := path.Split(filePath)

	matches := fileRegexp.FindAllStringSubmatch(fileName, -1)
	if matches == nil {
		return nil, errors.New("Unexpected file name: " + fileName)
	}

	metricType := matches[0][1]
	aggregation := matches[0][2]
	if aggregation == "" {
		aggregation = AggregateSum
	}
	pid := matches[0][3]

	return &FileMetadata{MetricType: metricType, Aggregation: aggregation, Pid: pid}, nil
}

// An entry consists of a 4 byte header containing the entry length plus
// the 8 byte aligned content (metric key and value).
func readNextSample(offset uint64, content string) (uint64, encodedSample, error) {
	encodedLen := uint64(binary.LittleEndian.Uint32([]byte(content[offset : offset+4])))
	if encodedLen == 0 {
		// no more metrics
		return 0, encodedSample{}, io.EOF
	}
	offset += 4

	if offset+encodedLen > uint64(len(content)) {
		return 0, encodedSample{}, errors.New("metric content is too long")
	}

	json := content[offset : offset+encodedLen]
	offset += encodedLen

	// Metric content is padded to 8 bytes; skip any remaining empty bytes.
	entryLen := entryHeaderLen + encodedLen
	padding := 8 - entryLen%8
	offset += padding

	if offset+8 > uint64(len(content)) {
		return 0, encodedSample{}, errors.New("metric is missing value")
	}

	value := math.Float64frombits(binary.LittleEndian.Uint64([]byte(content[offset : offset+8])))
	offset += 8

	sample := encodedSample{
		json:  json,
		value: value,
	}

	return offset, sample, nil
}
