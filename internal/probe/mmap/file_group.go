package mmap

type FileGroup struct {
	Typ string
	Agg string
}

// GroupFiles gathers files into logical groups of metric type and aggregation so that:
// (type1, agg1) -> [matching files]
// ...
// (typeN, aggM) -> [matching files]
func GroupFiles(files []string) (map[FileGroup][]string, error) {
	fileGroups := make(map[FileGroup][]string)
	for _, fpath := range files {
		md, err := metaDataFromPath(fpath)
		if err != nil {
			return nil, err
		}
		key := FileGroup{md.MetricType, md.Aggregation}
		group := fileGroups[key]
		group = append(group, fpath)
		fileGroups[key] = group
	}
	return fileGroups, nil
}
