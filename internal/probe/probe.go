package probe

import (
	"fmt"
	"strings"
)

const (
	MetricsNamespace    = "gitlab_metrics_exporter"
	MetricTypeCounter   = "counter"
	MetricTypeGauge     = "gauge"
	MetricTypeHistogram = "histogram"
	MetricTypeSummary   = "summary"
)

type Label struct {
	Name  string
	Value string
}

type Sample struct {
	FamilyName string // The metric family name. Ex.: http_request_duration_seconds
	MetricName string // The actual metric name. Ex.: http_request_duration_seconds_bucket
	Labels     LabelSlice
	Value      float64
}

type MetricGroup struct {
	Type    string
	Help    string
	Samples SampleSlice
}

type ProbeResult struct {
	MGroup MetricGroup
	Error  error
}

type LabelSlice []Label
type SampleSlice []Sample

type Probe interface {
	Probe(ch chan<- ProbeResult) error
	fmt.Stringer
}

func Await(p Probe) ([]MetricGroup, error) {
	ch := make(chan ProbeResult)

	go func() {
		defer close(ch)

		if err := p.Probe(ch); err != nil {
			ch <- ProbeResult{Error: err}
		}
	}()

	var mgroups []MetricGroup
	for res := range ch {
		if res.Error != nil {
			return nil, res.Error
		}
		mgroups = append(mgroups, res.MGroup)
	}

	return mgroups, nil
}

func (ss SampleSlice) Len() int {
	return len(ss)
}

func (ss SampleSlice) Swap(a, b int) {
	ss[a], ss[b] = ss[b], ss[a]
}

func (ss SampleSlice) Less(a, b int) bool {
	cmp := strings.Compare(ss[a].MetricName, ss[b].MetricName)
	if cmp < 0 {
		return true
	} else if cmp > 0 {
		return false
	}
	return compareLabels(ss[a].Labels, ss[b].Labels) < 0
}

func compareLabels(ls1, ls2 LabelSlice) int {
	var upTo int
	if len(ls1) <= len(ls2) {
		upTo = len(ls1)
	} else {
		upTo = len(ls2)
	}

	for i := 0; i < upTo; i++ {
		cmp := strings.Compare(ls1[i].Name, ls2[i].Name)

		if cmp != 0 {
			return cmp
		}
	}

	for i := 0; i < upTo; i++ {
		cmp := strings.Compare(ls1[i].Value, ls2[i].Value)

		if cmp != 0 {
			return cmp
		}
	}

	return 0
}

func (ls LabelSlice) Len() int {
	return len(ls)
}

func (ls LabelSlice) Swap(a, b int) {
	ls[a], ls[b] = ls[b], ls[a]
}

func (ls LabelSlice) Less(a, b int) bool {
	v1 := ls[a]
	v2 := ls[b]
	cmp := strings.Compare(v1.Name, v2.Name)

	switch {
	case cmp < 0:
		return true
	case cmp > 0:
		return false
	}

	return v1.Value < v2.Value
}
