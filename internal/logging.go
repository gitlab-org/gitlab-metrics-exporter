package internal

import (
	"io"
	goLog "log"
	"os"

	logkit "gitlab.com/gitlab-org/labkit/log"
)

func SetupLog(file string, format string, level string) (io.Closer, error) {
	// Golog always goes to stderr
	goLog.SetOutput(os.Stderr)

	if level == "quiet" {
		return logkit.Initialize(logkit.WithWriter(io.Discard))
	}

	return logkit.Initialize(
		logkit.WithOutputName(file),
		logkit.WithFormatter(format),
		logkit.WithLogLevel(level),
	)
}
