package internal

import (
	"strconv"
)

const compactDecimalPoints = -1
const preciseDecimalPoints = 17

func FormatCompactFloat(v float64) string {
	return strconv.FormatFloat(v, 'g', compactDecimalPoints, 64)
}

func AppendPreciseFloat(dst []byte, v float64) []byte {
	return strconv.AppendFloat(dst, v, 'g', preciseDecimalPoints, 64)
}
