# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

stages:
- build
- test

include:
- template: Security/License-Scanning.gitlab-ci.yml
- template: Security/Dependency-Scanning.gitlab-ci.yml
- template: Security/SAST.gitlab-ci.yml
- template: Security/Secret-Detection.gitlab-ci.yml
- project: 'gitlab-org/quality/pipeline-common'
  file: '/ci/danger-review.yml'

.go_build: &go_build
  image: golang:${GO_VERSION}
  parallel:
    matrix:
      - GO_VERSION: [1.19.9, 1.20.5]

build:
  <<: *go_build
  stage: build
  script:
  - make tidy all

lint:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
  stage: test
  script:
  - "[ -e .golangci.yml ] || cp /golangci/.golangci.yml ."
  - golangci-lint run --out-format code-climate | tee gl-code-quality-report.json
    | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
    - gl-code-quality-report.json

test-unit:
  <<: *go_build
  stage: test
  script:
  - make test-unit

test-acceptance:
  <<: *go_build
  stage: test
  before_script:
  - apt-get update && apt-get install -y ruby ruby-dev
  - gem install bundler
  script:
  - make test-acceptance

sast:
  <<: *go_build
  variables:
    SAST_EXCLUDED_PATHS: test, **/*_test.go
    SAST_EXCLUDED_ANALYZERS: bandit, brakeman, eslint, flawfinder, kubesec, nodejs-scan,
      phpcs-security-audit, pmd-apex, security-code-scan, semgrep, sobelow, spotbugs
  stage: test
