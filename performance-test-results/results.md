# Performance test results

Tests were run against GME `ee30d7b0bcdfc9dc8599649e8146e780b9458e79` and the Ruby exporter (gitlab-rails)
at `63a3ff10a74cd6c5b2d0ac2fc4b497b72d663f74`. We used the same production Puma metrics data set also
used for benchmarking in `test/data/mmap/prod-puma`. Tools used to run these tests and capture results
were Apache Bench 2.3 and [psrecord](https://github.com/astrofrog/psrecord). Tests were run on Fedora
36 kernel 5.19.6, ThinkPad X1 Carbon Gen 9 running an 11th gen Core i7 with 8 hardware threads @4GHz
(with the default governor so frequency scaling was _not_ disabled).

Note that GME has a connection limit of 1, meaning that it will never serve more than a single
request simultaneously. This is intentional since expected traffic from a scraper will be very
low, which is why we optimize for predicable memory and CPU use over throughput.

## Summary & Highlights

* With a single connected client, both exporters use a similar amount of memory and CPU,
  but GME is 3.3x faster in the 99th, serving 4 times more requests per second on average.
* With 10 concurrent requests, GME maintains its baseline CPU and memory use while
  Ruby exporter memory use grows to 300MB. This is interesting because the Ruby exporter
  uses WEBrick, a single-threaded web server that can only serve a single client at a time
  so I would have expected it to maintain a similar baseline.
* CPU usage in both GME and the Ruby exporter look stable over time over the duration of
  a single test run, though GME CPU use is slightly lower and spikier and GME memory use
  is slightly higher on average.

## Results

### GME - concurrency 1

```
ab -n 500 -c 1 -l localhost:8082/metrics
```

```
Concurrency Level:      1
Time taken for tests:   22.356 seconds
Complete requests:      500
Failed requests:        0
Total transferred:      2319298071 bytes
HTML transferred:       2319249571 bytes
Requests per second:    22.37 [#/sec] (mean)
Time per request:       44.711 [ms] (mean)
Time per request:       44.711 [ms] (mean, across all concurrent requests)
Transfer rate:          101314.39 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       0
Processing:    35   45   5.9     43      89
Waiting:        2    3   0.9      3       7
Total:         35   45   5.9     43      89

Percentage of the requests served within a certain time (ms)
  50%     43
  66%     46
  75%     49
  80%     49
  90%     53
  95%     55
  98%     58
  99%     59
 100%     89 (longest request)
```

![plot](plot-gme-c1.png)

### GME - concurrency 10

```
ab -n 500 -c 10 -l localhost:8082/metrics
```

```
Concurrency Level:      10
Time taken for tests:   22.077 seconds
Complete requests:      500
Failed requests:        0
Total transferred:      2319297484 bytes
HTML transferred:       2319248984 bytes
Requests per second:    22.65 [#/sec] (mean)
Time per request:       441.550 [ms] (mean)
Time per request:       44.155 [ms] (mean, across all concurrent requests)
Transfer rate:          102590.48 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       0
Processing:    44  437  38.7    440     498
Waiting:        7  396  38.2    399     458
Total:         44  437  38.6    440     498

Percentage of the requests served within a certain time (ms)
  50%    440
  66%    447
  75%    453
  80%    457
  90%    466
  95%    477
  98%    486
  99%    491
 100%    498 (longest request)
```

![plot](plot-gme-c10.png)

### Ruby - concurrency 1

```
ab -n 500 -c 1 -l localhost:3809/metrics
```

```
Concurrency Level:      1
Time taken for tests:   99.317 seconds
Complete requests:      500
Failed requests:        0
Total transferred:      2313003946 bytes
HTML transferred:       2312898446 bytes
Requests per second:    5.03 [#/sec] (mean)
Time per request:       198.633 [ms] (mean)
Time per request:       198.633 [ms] (mean, across all concurrent requests)
Transfer rate:          22743.35 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       0
Processing:   189  199   6.2    198     264
Waiting:      188  198   6.2    197     263
Total:        189  199   6.2    198     264

Percentage of the requests served within a certain time (ms)
  50%    198
  66%    199
  75%    201
  80%    201
  90%    204
  95%    209
  98%    215
  99%    218
 100%    264 (longest request)
```

![plot](plot-ruby-c1.png)

### Ruby - concurrency 10

```
ab -n 500 -c 10 -l localhost:3809/metrics
```

```
Concurrency Level:      10
Time taken for tests:   100.782 seconds
Complete requests:      500
Failed requests:        0
Total transferred:      2313012677 bytes
HTML transferred:       2312907177 bytes
Requests per second:    4.96 [#/sec] (mean)
Time per request:       2015.641 [ms] (mean)
Time per request:       201.564 [ms] (mean, across all concurrent requests)
Transfer rate:          22412.74 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       0
Processing:   595 2007 428.7   1996    3370
Waiting:      202 1339 374.6   1385    2424
Total:        595 2008 428.7   1997    3370

Percentage of the requests served within a certain time (ms)
  50%   1997
  66%   2184
  75%   2230
  80%   2383
  90%   2583
  95%   2748
  98%   2952
  99%   3176
 100%   3370 (longest request)
```

![plot](plot-ruby-c10.png)
