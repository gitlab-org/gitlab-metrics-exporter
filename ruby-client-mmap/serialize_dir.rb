#!/usr/bin/env ruby
# frozen-string-literal: true

require 'bundler/setup'
require 'prometheus/client'
require 'prometheus/client/formats/text'

puts ::Prometheus::Client::Formats::Text.marshal_multiprocess(ARGV[0])
