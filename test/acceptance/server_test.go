package acceptance

import (
	"io"
	"net/http"
	"path"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-metrics-exporter/internal/testhelpers"
)

const (
	testServerAddress = "localhost:8082"
)

func Test_Acceptance_GetMetrics(t *testing.T) {
	tests := []struct {
		client   *http.Client
		endpoint string
		flags    []string
	}{
		{testhelpers.HTTPClient(t), "http://" + testServerAddress + "/metrics", []string{"-p", "self"}},
		{testhelpers.HTTPSClient(t), "https://" + testServerAddress + "/metrics", []string{
			"-p", "self",
			"--cert-file", path.Join(testhelpers.CertsDir, "cert.pem"),
			"--cert-key", path.Join(testhelpers.CertsDir, "key.pem"),
		}},
	}

	for _, tc := range tests {
		server, err := testhelpers.StartServer(tc.flags...)
		require.NoError(t, err)

		func() {
			defer server.Stop()

			require.NoError(t, testhelpers.Await(func() bool {
				resp, err := tc.client.Get(tc.endpoint)
				// Make sure we close the response body after the first successful request
				// since otherwise we may be blocking subsequent requests from being processed.
				if resp != nil {
					defer resp.Body.Close()
				}

				return err == nil
			}))

			resp, err := tc.client.Get(tc.endpoint)
			require.NoError(t, err)
			defer resp.Body.Close()
			require.Equal(t, 200, resp.StatusCode)

			bytes, err := io.ReadAll(resp.Body)
			require.NoError(t, err)
			require.NotEmpty(t, bytes)
			require.True(t, strings.HasPrefix(string(bytes), "# HELP"))
		}()
	}
}
